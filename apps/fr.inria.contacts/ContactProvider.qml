/****************************************************************************
**
** Author (C) Christian Frisson
** Copyright (C) 2017 Inria
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Neptune IVI UI.
** fr.inria.contacts is forked from com.pelagicore.music
**
** $QT_BEGIN_LICENSE:GPL-QTAS$
** Commercial License Usage
** Licensees holding valid commercial Qt Automotive Suite licenses may use
** this file in accordance with the commercial license agreement provided
** with the Software or, alternatively, in accordance with the terms
** contained in a written agreement between you and The Qt Company.  For
** licensing terms and conditions see https://www.qt.io/terms-conditions.
** For further information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
** SPDX-License-Identifier: GPL-3.0
**
****************************************************************************/

pragma Singleton
import QtQuick 2.1
import QtApplicationManager 1.0
import com.pelagicore.datasource 1.0
import service.contact 1.0

QtObject {
    id: root

    property SqlQueryDataSource contactLibrary: SqlQueryDataSource {
        database: "contacts"
        query: 'select * from contacts'
    }

    property SqlQueryDataSource nowCalling: SqlQueryDataSource {
        database: "contacts"
        query: 'select distinct * from contacts order by random()'
        onQueryChanged: root.reevaluate()
    }

    property int currentIndex: 0
    property int count: nowCalling.count

    property var currentEntry: nowCalling.get(currentIndex);
    property url currentLogo: nowCalling.storageLocation + '/contacts/' + currentEntry.logo
    property url currentImage: nowCalling.storageLocation + '/contacts/' + currentEntry.image

    function reevaluate() {
        currentIndex = -1
        currentIndex = 0
    }

    function queryAllLabels() {
        contactLibrary.query = 'select * from contacts group by label'
    }

    function queryPeople() {
        contactLibrary.query = 'select distinct * from contacts'
    }

    function querySpecLabel(label) {
        nowCalling.query = "select distinct * from contacts where label='" + label + "'"
    }

    function selectSpecPerson () {
        nowCalling.query = 'select distinct * from contacts'
    }

    function selectRandomPersons() {
        nowCalling.query = 'select distinct * from contacts order by random()'
    }

    function imagePath(image) {
        return Qt.resolvedUrl(root.nowCalling.storageLocation + '/contacts/' + image)
    }

    function logoPath(logo) {
        return Qt.resolvedUrl(root.nowCalling.storageLocation + '/contacts/' + logo)
    }

    function next() {
        print('ContactService.nextPerson()')
        if (root.currentIndex < root.count - 1)
            currentIndex++
    }

    function previous() {
        print('ContactService.previousPerson()')
        if (currentIndex > 0)
            currentIndex--
    }

    function initialize() {
        ContactService.contactProvider = root
        ContactService.currentIndex = Qt.binding(function() { return root.currentIndex})
        ContactService.currentPerson = Qt.binding(function() { return root.currentEntry})
        ContactService.personCount = Qt.binding(function() { return root.nowCalling.count})
        ContactService.imagePath = Qt.binding(function() { return root.currentImage})
        ContactService.logoPath = Qt.binding(function() { return root.currentLogo})
        console.log("ContactProvider initialize")
    }


    property Item ipc: Item {
        ApplicationInterfaceExtension {
            id: contactRemoteControl

            name: "fr.inria.contact.control"
        }

        Binding { target: contactRemoteControl.object; property: "currentPerson"; value: ContactService.currentPerson }
        Binding { target: contactRemoteControl.object; property: "currentTime"; value: ContactService.currentTime }
        Binding { target: contactRemoteControl.object; property: "durationTime"; value: ContactService.durationTime }
        Binding { target: contactRemoteControl.object; property: "playing"; value: ContactService.playing }

        Connections {
            target: contactRemoteControl.object

            onPlay: {
                ContactService.contactPlay()
            }

            onPause: {
                ContactService.pause()
            }

            onPreviousPerson: {
                ContactService.previousPerson()
            }

            onNextPerson: {
                ContactService.nextPerson()
            }
        }
    }

    Component.onCompleted: {
        print("ContactProvider completed", root.count)

    }
}
