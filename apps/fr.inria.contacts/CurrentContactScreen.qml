/****************************************************************************
**
** Author (C) Christian Frisson
** Copyright (C) 2017 Inria
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Neptune IVI UI.
** fr.inria.contacts is forked from com.pelagicore.music
**
** $QT_BEGIN_LICENSE:GPL-QTAS$
** Commercial License Usage
** Licensees holding valid commercial Qt Automotive Suite licenses may use
** this file in accordance with the commercial license agreement provided
** with the Software or, alternatively, in accordance with the terms
** contained in a written agreement between you and The Qt Company.  For
** licensing terms and conditions see https://www.qt.io/terms-conditions.
** For further information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
** SPDX-License-Identifier: GPL-3.0
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.0

import controls 1.0
import utils 1.0
import service.contact 1.0
import "."

UIScreen {
    id: root
    hspan: 24
    vspan: 24

    title: 'Contact'

    property var person: ContactProvider.currentEntry
    property bool libraryVisible: false

    signal showLabels()

    ColumnLayout {
        id: contactControl
        width: Style.hspan(12)
        height: Style.vspan(20)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 0
        Spacer {}
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0
            Tool {
                hspan: 2
                name: 'prev'
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: Style.vspan(-1)
                onClicked: {
                    if (ContactProvider.currentIndex === 0)
                        ContactProvider.currentIndex = ContactProvider.count - 1
                    else
                        ContactProvider.currentIndex --
                }
            }

            SwipeView {
                id: pathView
                itemWidth: Style.cellWidth * 6

                width: Style.cellWidth * 6
                height: Style.cellHeight * 12

                items: ContactProvider.nowCalling.model

                currentViewIndex: ContactProvider.currentIndex

                delegate: ImageItem {
                    z: PathView.z
                    scale: PathView.scale
                    source: ContactProvider.imagePath(model.image)
                    title: model.person
                    subTitle: model.label
                    onClicked: {
                        root.showLabels()
                    }
                }

            }

            Tool {
                hspan: 2
                name: 'next'
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: Style.vspan(-1)
                onClicked: {
                    if (ContactProvider.currentIndex === ContactProvider.count - 1)
                        ContactProvider.currentIndex = 0
                    else
                        ContactProvider.currentIndex ++
                }
            }
        }

        /*RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10

            Label {
                hspan: 2
                text: ContactService.currentTime
                font.pixelSize: Style.fontSizeS
            }
        }*/

        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0
            Tool {
                hspan: 2
                name: 'phone'
                onClicked: ContactService.togglePlay()
            }
        }
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0
            Symbol {
                vspan: 2
                hspan: 1
                size: Style.symbolSizeXS
                name: 'speaker'
            }
            VolumeSlider {
                hspan: 8
                vspan: 2
                anchors.horizontalCenter: parent.horizontalCenter
                value: ContactService.volume
                onValueChanged: {
                    ContactService.volume = value
                }
            }
            Label {
                hspan: 1
                vspan: 2
                text: Math.floor(ContactService.volume*100)
            }
        }
        Spacer {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }

    ContactLibrary {
        id: library
        //x: parent.width
        x: -library.width
        opacity: 0
        visible: opacity > 0
        onClose: {
            root.libraryVisible = false
        }
    }
/*
    UIElement {
        id: sourceOption
        hspan: 4
        vspan: 12
        anchors.right: contactControl.left
        anchors.rightMargin: 60
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -60

        Column {
            spacing: 1
            Button {
                hspan: 4
                vspan: 4
                text: "BLUETOOTH"
                label.font.pixelSize: Style.fontSizeL
            }
            Button {
                hspan: 4
                vspan: 4
                text: "USB"
                enabled: false
                label.font.pixelSize: Style.fontSizeL
            }
            Button {
                hspan: 4
                vspan: 4
                text: "SPOTIFY"
                enabled: false
                label.font.pixelSize: Style.fontSizeL
            }
        }
    }
*/
    TextTool {
        id: libraryButton
        hspan: 3
        vspan: 5
        //anchors.left: contactControl.right
        anchors.right: contactControl.left
        //anchors.leftMargin: 30
        anchors.rightMargin: 30
        anchors.verticalCenter: parent.verticalCenter
        size: 72

        name: "contact"
        text: "CONTACTS"

        onClicked: root.libraryVisible = true
    }

    Component.onCompleted: ContactProvider.selectRandomPersons()

    states: State {
        name: "libaryMode"; when: root.libraryVisible

        PropertyChanges {
            target: library
            opacity: 1
            //x: root.width - library.width
            x: 0
        }

        PropertyChanges {
            target: libraryButton
            opacity: 0
        }

        PropertyChanges {
            target: sourceOption
            opacity: 0
        }

        AnchorChanges {
            target: contactControl
            anchors.horizontalCenter: undefined
        }

        PropertyChanges {
            target: contactControl
            //x: 0
            x: library.width
        }
    }

    transitions: Transition {
        from: ""; to: "libaryMode"; reversible: true

        ParallelAnimation {
            NumberAnimation { target: library; properties: "opacity"; duration: 400 }
            NumberAnimation { target: library; properties: "x"; duration: 300 }
            NumberAnimation { target: libraryButton; properties: "opacity"; duration: 300 }
            NumberAnimation { target: sourceOption; properties: "opacity"; duration: 300 }
            NumberAnimation { target: contactControl; properties: "x"; duration: 300 }
        }
    }

}
