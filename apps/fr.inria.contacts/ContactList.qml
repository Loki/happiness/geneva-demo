/****************************************************************************
**
** Author (C) Christian Frisson
** Copyright (C) 2017 Inria
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Neptune IVI UI.
** fr.inria.contacts is forked from com.pelagicore.music
**
** $QT_BEGIN_LICENSE:GPL-QTAS$
** Commercial License Usage
** Licensees holding valid commercial Qt Automotive Suite licenses may use
** this file in accordance with the commercial license agreement provided
** with the Software or, alternatively, in accordance with the terms
** contained in a written agreement between you and The Qt Company.  For
** licensing terms and conditions see https://www.qt.io/terms-conditions.
** For further information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
** SPDX-License-Identifier: GPL-3.0
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.0

import controls 1.0
import utils 1.0
import service.contact 1.0
import "."

UIElement {
    id: root
    hspan: 10
    vspan: 18

    property string type: ""
    property bool nowCalling: false

    onTypeChanged: {
        if (type === "people")
            ContactProvider.queryPeople()
        else if (type === "labels")
            ContactProvider.queryAllLabels()
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
    }

    ListView {
        id: listView
        anchors.fill: parent
        anchors.topMargin: 10
        anchors.bottomMargin: 84
        model: root.nowCalling ? ContactProvider.nowCalling.model : ContactProvider.contactLibrary.model
        clip: true
        highlightMoveDuration: 300
        highlightFollowsCurrentItem: false
        currentIndex: ContactProvider.currentIndex

        delegate: UIElement {
            hspan: root.hspan
            vspan: 3

            Rectangle {
                anchors.fill: parent
                opacity: 0.2
                visible: listView.currentIndex === index
            }

            Rectangle {
                width: parent.width
                height: 1
                opacity: 0.2
                color: "white"
            }

            Row {
                anchors.verticalCenter: parent.verticalCenter
                Icon {
                    hspan: 2
                    vspan: hspan
                    anchors.verticalCenter: parent.verticalCenter
                    fit: true
                    source: root.type === "labels" ? ContactProvider.imagePath(model.logo) : ContactProvider.imagePath(model.image)
                }

                Column {
                    Label {
                        hspan: 7
                        vspan: 1
                        visible: root.type !== "labels"
                        text: root.type === "labels" ? '' : model.person
                        font.pixelSize: Style.fontSizeM
                    }
                    Label {
                        hspan: 7
                        vspan: root.type === "labels" ? 2 : 1
                        text: model.label
                        font.pixelSize: Style.fontSizeS
                        font.bold: true
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (root.nowCalling) {
                        ContactProvider.currentIndex = index
                    }
                    else {
                        listView.currentIndex = index
                        if (root.type === "people") {
                            ContactProvider.selectSpecPerson()
                            ContactProvider.currentIndex = index
                        }
                        else if (root.type === "labels")
                            ContactProvider.querySpecLabel(model.label)
                    }
                    ContactService.play()
                }
            }
        }
    }
}
