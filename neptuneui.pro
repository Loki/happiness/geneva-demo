include(doc/doc.pri)
TEMPLATE = subdirs

SUBDIRS = plugins/datasource \
          plugins/screenmanager \
          plugins/comtqci18ndemo \
          plugins/touchprovider

qml.files = apps imports sysui i18n am-config.yaml Main*.qml

INSTALLS += qml
