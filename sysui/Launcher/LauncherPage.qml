/****************************************************************************
**
** Copyright (C) 2016 Pelagicore AG
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Neptune IVI UI.
**
** $QT_BEGIN_LICENSE:GPL-QTAS$
** Commercial License Usage
** Licensees holding valid commercial Qt Automotive Suite licenses may use
** this file in accordance with the commercial license agreement provided
** with the Software or, alternatively, in accordance with the terms
** contained in a written agreement between you and The Qt Company.  For
** licensing terms and conditions see https://www.qt.io/terms-conditions.
** For further information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
** SPDX-License-Identifier: GPL-3.0
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.0

import controls 1.0
import utils 1.0

import QtApplicationManager 1.0
import fr.inria.touchprovider 1.0
import models 1.0

UIPage {
    id: root
    hspan: 24
    vspan: 21

    title: qsTr("")
    //symbolName: "apps"

    signal updateApp()

    GridView {
        id: view

        property bool editMode: false
        property int columns: 3//root.width/Style.hspan(4)

        anchors.top: parent.top
        anchors.topMargin: Style.vspan(3)
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: cellWidth*columns

        clip: true
        cellWidth: Style.hspan(4)
        cellHeight: Style.vspan(7)

        model: ListModel {
            ListElement {
                applicationId: "fr.inria.settings"
            }
            ListElement {
                applicationId: "fr.inria.navigation"
            }
            ListElement {
                applicationId: "fr.inria.contacts"
            }
            ListElement {
                applicationId: "com.pelagicore.music"
            }
            ListElement {
                applicationId: "fr.inria.climate"
            }
            ListElement {
                applicationId: "com.pelagicore.browser"
            }
        }

        flickableChildren: MouseArea {
            anchors.fill: parent
            onClicked: view.editMode = false
        }

        add: Transition {
            NumberAnimation { properties: "opacity, scale"; from: 0; to: 1 }
        }

        remove: Transition {
            NumberAnimation { properties: "opacity, scale"; to: 0 }
        }

        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 200 }
        }

        delegate: AppButton {
            width: view.cellWidth
            height: view.cellHeight

            name: ApplicationManager.get(model.applicationId).name
            icon: Qt.resolvedUrl(ApplicationManager.get(model.applicationId).icon)

            editMode: view.editMode
            removable: ApplicationManager.get(model.applicationId).isRemovable
            installProgress: ApplicationManager.get(model.applicationId).updateProgress
            isUpdating: ApplicationManager.get(model.applicationId).isUpdating

            onIsUpdatingChanged: {
                if (isUpdating)
                    root.updateApp()
            }

            onClicked: {
                if (view.editMode) {
                    view.editMode = false
                } else {
                    if (!ApplicationManager.get(model.applicationId).isUpdating)
                        console.log("Starting app " + model.applicationId + ": " + ApplicationManager.startApplication(model.applicationId));
                }
            }

            onRemoveClicked: {
                ApplicationInstaller.removePackage(model.applicationId, false /*keepDocuments*/, true /*force*/)
            }
            onPressAndHold: view.editMode = true
        }
    }

    Connections {
        target: Touchprovider
        onButtonPress: {
            switch (Touchprovider.currentBtn) {
                case "1":
                    //Browser
                    ApplicationManager.startApplication("com.pelagicore.browser")
                    break;
                case "2":
                    //Climate
                    ApplicationManager.startApplication("fr.inria.climate")
                    break;
                case "3":
                    ///Music
                    ApplicationManager.startApplication("com.pelagicore.music")
                    break;
                case "4":
                    //Settings
                    ApplicationManager.startApplication("fr.inria.settings")
                    break;
                case "5":
                    //Navigation
                    ApplicationManager.startApplication("fr.inria.navigation")
                    break;
                case "6":
                    //Contact
                    ApplicationManager.startApplication("fr.inria.contacts")
                    break;
            }
        }
    }
}
