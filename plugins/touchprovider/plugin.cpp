#include "plugin.h"

#include <qqml.h>

#include "touchprovider.h"

QObject *getInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return Touchprovider::instance();
}

void Plugin::registerTypes(const char *uri)
{
    // @uri fr.inria.touchprovider
    qmlRegisterSingletonType<Touchprovider>(uri, 1, 0, "Touchprovider", &getInstance);
}


