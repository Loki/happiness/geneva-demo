#ifndef CAP1188_HANDLER_H
#define CAP1188_HANDLER_H

#include <QObject>
#include <QTimer>
#include "happinesslib/cap1188.h"
#include "happinesslib/cap1214.h"
#include "happinesslib/drv2667.h"
#include "happinesslib/tca9548a.h"
#include <QSettings>
#include "irqhandler.h"

class Touchprovider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentBtn READ getCurrentBtn())
    Q_PROPERTY(float smallSliderValue READ getSmallSliderValue())
    Q_PROPERTY(int smallSliderResolution READ getSmallSliderResolution())

    Q_PROPERTY(float bigSliderValue READ getBigSliderValue())
    Q_PROPERTY(int bigSliderResolution READ getBigSliderResolution())
public:
    static Touchprovider* instance();
    explicit Touchprovider(QObject *parent = 0);

    QString getCurrentBtn();

    float getSmallSliderValue();
    int getSmallSliderResolution();

    float getBigSliderValue();
    int getBigSliderResolution();
private:
    QString _currentBtn;

    float _smallSliderValue;
    int _smallSliderResolution;

    float _bigSliderValue;
    int _bigSliderResolution;

    int state; //0 released 1 pressed
    cap1188_t buttonCap;
    cap1188_t smallSliderCap;
    cap1214_t bigSliderCap;

    drv2667_t drv;

    tca9548a_t tca;

    int vibrate(int buttonInd);

    IRQHandler buttonIrqH = {GPIO_4, LOW, RISING};
    IRQHandler smallSliderIrqH = {GPIO_18, LOW, RISING};
    IRQHandler bigSliderIrqH = {GPIO_23, HIGH, RISING};
signals:
    void buttonPress(int button);
    void buttonRelease(int button);
    void smallSliderTouch();
    void bigSliderTouch();
public slots:
    void buttonIRQhappened();
    void smallSliderIRQhappened();
    void bigSliderIRQhappened();
    /// Slot to receive events from the touchpad of the "key"
    void keyPadIRQhappened();
    void keyPadPressed();
    void keyPadReleased();
private:
    int keyPointerFrame;
};

#endif // CAP1188_HANDLER_H
