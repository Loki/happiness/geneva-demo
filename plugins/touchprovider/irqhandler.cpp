#include "irqhandler.h"

IRQHandler::IRQHandler(gpio_pin_t pin, gpio_active_t act, gpio_edge_t edge) : QThread()
{
    //Init IRQ
    gpio_irq_init(&irq, pin, act, edge);
}

void IRQHandler::run() {
    waitForIRQ(&irq);
    emit notifyIRQ();
}

