TEMPLATE = lib
TARGET  = touchproviderplugin
QT += qml quick
CONFIG += qt plugin c++11

TARGET = $$qtLibraryTarget($$TARGET)
uri = fr.inria.touchprovider

SOURCES += \
    plugin.cpp \
    irqhandler.cpp \
    touchprovider.cpp \
    happinesslib/cap1188.cpp \
    happinesslib/gpio_irq.cpp \
    happinesslib/i2cmple.cpp \
    happinesslib/cap1214.cpp \
    happinesslib/drv2667.cpp \
    happinesslib/tca9548a.cpp

HEADERS += \
    plugin.h \
    irqhandler.h \
    touchprovider.h \
    happinesslib/cap1188.h \
    happinesslib/gpio_irq.h \
    happinesslib/i2cmple.h \
    happinesslib/cap1214.h \
    happinesslib/drv2667.h \
    happinesslib/tca9548a.h

OTHER_FILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir TouchProvider.qml
installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
qmldir.path = $$installPath
target.path = $$installPath
INSTALLS += target qmldir

DISTFILES +=

