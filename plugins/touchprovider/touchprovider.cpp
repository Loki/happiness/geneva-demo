#include "touchprovider.h"
#include <QDebug>
#include <QCursor>
#include <QGuiApplication>
#include <QScreen>
#include <QMouseEvent>
#include <QWindow>

Touchprovider::Touchprovider(QObject *parent) : QObject(parent) {
    _currentBtn = "0";
    state = 0;

    //Loading settings
    QSettings settings("./plugins/touchprovider/touchsettings.ini", QSettings::NativeFormat);

    _smallSliderResolution = settings.value("smallSliderResolution", 100).toInt();
    _bigSliderResolution = settings.value("bigSliderResolution", 100).toInt();

    uint8_t buff[8];
    int r;
    //Init button cap
    r = cap1188_init(&buttonCap, ADDR_DEFAULT);
    if (r < 0) { qDebug() << "Error during button cap1188 init"; }
    r = cap1188_set_sensor_sensitivity(&buttonCap, settings.value("buttonSensitivity", 0b00000001).toUInt());
    if (r < 0) { qDebug() << "Error while setting sensitivity for button cap1188"; }
    memset(buff, settings.value("buttonThreshold", 0x04).toUInt(), 8);
    r = cap1188_set_sensor_threshold(&buttonCap, buff);
    if (r < 0) { qDebug() << "Error while threshold for cap1188 button"; }
    r = cap1188_set_interrupt_repeat(&buttonCap, 0x00);
    if (r < 0) { qDebug() << "Error while setting interrupt repeat for button cap1188"; }

    //Init small slider cap
    r = cap1188_init(&smallSliderCap, ADDR_180K);
    if (r < 0) { qDebug() << "Error during small slider cap1188 init"; }
    r = cap1188_set_sensor_sensitivity(&smallSliderCap, settings.value("smallSliderSensitivity", 0b00000100).toUInt());
    if (r < 0) { qDebug() << "Error while setting sensitivity for small slider cap1188"; }
    memset(buff,  settings.value("smallSliderThreshold", 0x05).toUInt(), 8);
    r = cap1188_set_sensor_threshold(&smallSliderCap, buff);
    if (r < 0) { qDebug() << "Error while setting threshold for small slider cap1188"; }

    //Init big slider cap
    r = cap1214_init(&bigSliderCap);
    if (r < 0) { qDebug() << "Error during big slider cap1214 init"; }
    r = cap1214_set_sensor_sensitivity(&bigSliderCap, settings.value("bigSliderSensitivity", 0b00000100).toUInt());
    if (r < 0) { qDebug() << "Error while setting sensitivity for big slider cap1214"; }
    memset(buff,  settings.value("bigSliderThreshold", 0x05).toUInt(), 8);
    r = cap1214_set_sensor_threshold(&bigSliderCap, buff);
    if (r < 0) { qDebug() << "Error while setting threshold for big slider cap1214"; }

    //Init tca
    r = tca9548a_init(&tca);
    if (r < 0) { qDebug() << "Error during tca9548 init"; }
    r = tca9548a_select(&tca, 0x00);
    if (r < 0) { qDebug() << "Error while selecting channel for tca9548"; }

    //Init drv
    //Prepare playlist
    uint8_t playlist[8];
    memset(playlist, 0x00, 8);
    playlist[0] = 0x01;
    //Prepare header
    drv2667_header_t header;
    drv2667_effect_t effects[1];
    header.neffect = 1;
    header.effects = effects;
    header.effects[0].start_addr_upper = 0x81;
    header.effects[0].start_addr_lower = 0x00;
    header.effects[0].stop_addr_upper = 0x01;
    header.effects[0].stop_addr_lower = 0x03;
    header.effects[0].repeat_count = 0x01;
    //Prepare waveform data
    drv2667_waveform_synthesis_t waveform;
    waveform.amplitude = 0xFF;
    waveform.frequency = 0x19;
    waveform.duration = 0x0A;
    waveform.envelope = 0x00;
    for (int i=0; i < 8;i++) {
        r = tca9548a_select(&tca, 1 << i);
        if (r < 0) { qDebug() << "Error while selecting channel for tca9548"; }
        r = drv2667_init_digital(&drv, DRV2667_50V_34_8DB, DRV2667_TO_5MS);
        if (r < 0) { qDebug() << "Error during init of drv2267 n°" + (i+1); }
        //Write playlist
        r = drv2667_set_playlist(&drv, playlist);
        if (r < 0) { qDebug() << "Error while setting playlist for drv2267 n°" + (i+1); }
        //Write ram data
        r = drv2667_write_ram_data(&drv, header, &waveform, 1);
        if (r < 0) { qDebug() << "Error while writing ram of drv2267 n°" + (i+1); }
    }
    r = tca9548a_select(&tca, 0xFF);
    if (r < 0) { qDebug() << "Error while selecting channel for tca9548"; }

    //Init button IRQ
    r = cap1188_clear_interrupt_bit(&buttonCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for button cap1188"; }
    buttonIrqH.start();
    connect(&buttonIrqH, SIGNAL(notifyIRQ()), this, SLOT(buttonIRQhappened()));
    //Init small slider IRQ
    r = cap1188_clear_interrupt_bit(&smallSliderCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for small slider cap1188"; }
    smallSliderIrqH.start();
    connect(&smallSliderIrqH, SIGNAL(notifyIRQ()), this, SLOT(smallSliderIRQhappened()));
    //Init big slider IRQ
    r = cap1214_clear_interrupt_bit(&bigSliderCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for big slider cap1214"; }
    bigSliderIrqH.start();
    connect(&bigSliderIrqH, SIGNAL(notifyIRQ()), this, SLOT(bigSliderIRQhappened()));

    //Init timer to test cursor movements from the "key" touchpad events
    keyPointerFrame = 0;
    /*QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(keyPadIRQhappened()));
    timer->start(1); // parameter: timer period in ms
    qDebug() << "Init timer to test cursor movements from the key touchpad events";
    */
}

Touchprovider* Touchprovider::instance() {
    static Touchprovider* obj = new Touchprovider();
    return obj;
}

QString Touchprovider::getCurrentBtn() {
    return _currentBtn;
}

float Touchprovider::getSmallSliderValue() {
    return _smallSliderValue;
}

int Touchprovider::getSmallSliderResolution() {
    return _smallSliderResolution;
}

float Touchprovider::getBigSliderValue() {
    return _bigSliderValue;
}

int Touchprovider::getBigSliderResolution() {
    return _bigSliderResolution;
}

int Touchprovider::vibrate(int buttonInd) {
    int r;
    uint8_t actuator = 0xFF;
    //Select button
    switch(buttonInd) {
        case 1:
            actuator = 0x01;
            break;
        case 2:
            actuator = 0x08;
            break;
        case 3:
            actuator = 0x20;
        break;
        case 4:
            actuator = 0x10;
        break;
        case 5:
            actuator = 0x04;
        break;
        case 6:
            actuator = 0x02;
        break;
    }
    r = tca9548a_select(&tca, actuator);
    if (r < 0) { qDebug() << "Error while selecting channel for tca9548"; }
    //Vibrate
    r = drv2667_play(&drv);
    if (r < 0) { qDebug() << "Error while trying to run play on drv2267"; }
    return r;
}

void Touchprovider::smallSliderIRQhappened() {
    int r;
    r = cap1188_read_cursor_position(&smallSliderCap, &_smallSliderValue, _smallSliderResolution);
    if (r < 0) { qDebug() << "Error while reading cursor position for small slider cap1188"; }
    emit smallSliderTouch();
    r = cap1188_clear_interrupt_bit(&smallSliderCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for small slider cap1188"; }
    smallSliderIrqH.start();
}

void Touchprovider::bigSliderIRQhappened() {
    int r;
    r = cap1214_read_cursor_position(&bigSliderCap, &_bigSliderValue, _bigSliderResolution);
    if (r < 0) { qDebug() << "Error while reading cursor position for big slider cap1214"; }
    emit bigSliderTouch();
    r = cap1214_clear_interrupt_bit(&bigSliderCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for big slider cap1214"; }
    bigSliderIrqH.start();
}

void Touchprovider::buttonIRQhappened() {
    int r;
    uint8_t buff;
    r = cap1188_read_sensors_satus(&buttonCap, &buff);
    if (r < 0) { qDebug() << "Error while reading sensors status for button cap1188"; }
    for (int i=0; i<6; i++) {
        //Since whe only want 1 button at the same time
        if ((buff & (1 << i)) > 0) {
            _currentBtn = QString::number((i+1));
            vibrate(i+1);
            break;
        }
    }

    //TODO fix this ugly handling of press/release
    if (state == 0) {
        state = 1;
        emit buttonPress(0);
    } else {
        state = 0;
        emit buttonRelease(0);
    }
    r = cap1188_clear_interrupt_bit(&buttonCap);
    if (r < 0) { qDebug() << "Error while cleaning interrupt bit for button cap1188"; }
    buttonIrqH.start();
}

void Touchprovider::keyPadIRQhappened(){
    int keyPointerFrameLoop = 1000;
    keyPointerFrame++;

    //qDebug() << "keyPointerIRQhappened ";

    // Test cursor movement
    /*int x = QGuiApplication::primaryScreen()->size().width() * ((float)keyPointerFrame/(float)keyPointerFrameLoop);
    int y = QGuiApplication::primaryScreen()->size().height() * ((float)keyPointerFrame/(float)keyPointerFrameLoop);
    QCursor cursor;
    cursor.setPos(x,y);
    QGuiApplication::setOverrideCursor(cursor);*/

    // Test click
    /*if(keyPointerFrame == 500){
        keyPadPressed();
        QTimer::singleShot(200, this, SLOT(keyPadReleased()));
    }*/

    keyPointerFrame %= keyPointerFrameLoop;
}

void Touchprovider::keyPadPressed(){
    QWindow* focusWindow = QGuiApplication::focusWindow();
    if(focusWindow){
        qDebug() << "keyPadPressed ";
        QPoint p = focusWindow->mapFromGlobal(QCursor::pos());
        QMouseEvent mousePress(QEvent::MouseButtonPress,p,Qt::LeftButton,Qt::LeftButton, Qt::NoModifier);
        QGuiApplication::sendEvent( (QObject*) QGuiApplication::focusWindow(),&mousePress );
    }
}

void Touchprovider::keyPadReleased(){
    QWindow* focusWindow = QGuiApplication::focusWindow();
    if(focusWindow){
        qDebug() << "keyPadReleased ";
        QPoint p = focusWindow->mapFromGlobal(QCursor::pos());
        QMouseEvent mouseRelease(QEvent::MouseButtonRelease,p,Qt::LeftButton,Qt::LeftButton, Qt::NoModifier);
        QGuiApplication::sendEvent( (QObject*) QGuiApplication::focusWindow(),&mouseRelease );
    }
}
