#ifndef IRQHANDLER_H
#define IRQHANDLER_H

#include <QThread>
#include "happinesslib/gpio_irq.h"


class IRQHandler : public QThread {
    Q_OBJECT

public:
    IRQHandler(gpio_pin_t pin, gpio_active_t act, gpio_edge_t edge);
protected:
  void run();
private:
    gpio_irq_t irq;
signals:
    void notifyIRQ();
public slots:
};

#endif // IRQHANDLER_H
