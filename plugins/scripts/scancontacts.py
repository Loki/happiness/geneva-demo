# -*- coding: utf-8 -*-

"""
/****************************************************************************
**
** Author (C) Christian Frisson
** Copyright (C) 2017 Inria
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Neptune IVI UI.
**
** $QT_BEGIN_LICENSE:GPL-QTAS$
** Commercial License Usage
** Licensees holding valid commercial Qt Automotive Suite licenses may use
** this file in accordance with the commercial license agreement provided
** with the Software or, alternatively, in accordance with the terms
** contained in a written agreement between you and The Qt Company.  For
** licensing terms and conditions see https://www.qt.io/terms-conditions.
** For further information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
** SPDX-License-Identifier: GPL-3.0
**
****************************************************************************/
"""

# scancontacts.py
# scans the ~/media/contacts folder
# and a ~/contacts.db database with all contacts.

import os
import sqlite3 as db
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='scan contacts folder')

parser.add_argument('--source', default='~/contacts', help='contacts dir to scan')

args = parser.parse_args()

source = os.path.expanduser(args.source)

labels = set()

connection = db.connect(os.path.expanduser('~/contacts.db'))
connection.text_factory = str
cursor = connection.cursor()


sql_drop = 'drop table if exists contacts'

sql_create = """
create table contacts (
    pk integer primary key,
    label text,
    person text,
    logo text,
    image text
    )
"""

sql_insert = """
insert into contacts (label,person,logo,image) VALUES (?,?,?,?)
"""

def createTable():
    print('create table')
    cursor.execute(sql_drop)
    cursor.execute(sql_create)

createTable()

df = pd.read_csv( os.path.join(source, 'contacts.csv'),encoding='utf-16');
folderpath = os.path.relpath(source, source)
for index, row in df.iterrows():
    label = ''
    title = ''
    person = ''
    logo = ''
    image = ''
    print(index,row['Name'],row['Organization 1 - Name'],row['Portrait File'],row['Organization 1 - Symbol'])
    PortraitFile = row['Portrait File']
    if not pd.isnull(PortraitFile):
        image = os.path.join(folderpath, PortraitFile);
    OrganizationSymbol = row['Organization 1 - Symbol']
    if not pd.isnull(OrganizationSymbol):
        logo = os.path.join(folderpath, OrganizationSymbol);
    Name = row['Name']
    hasName = not pd.isnull(Name);
    Organization = row['Organization 1 - Name']
    hasOrganization = not pd.isnull(Organization);
    if hasName and hasOrganization:
        person = Name;
        label = Organization;
    cursor.execute(sql_insert, (label, person, logo, image))

connection.commit()
